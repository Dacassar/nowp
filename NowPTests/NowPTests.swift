//
//  NowPTests.swift
//  NowPTests
//
//  Created by Евгений Браницкий on 10.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import XCTest
@testable import NowP

class TrackTests: XCTestCase {
    func testStringDuration() {
        let timeStr = "3:59"
        let duration: TimeInterval = 239
        let track = iTunesProvider.Track(info: ["Total Time":timeStr])
        XCTAssertEqual(duration, track.totalTime, "Time calculation wrong!")
    }
    func testTimeIntervalDuration() {
        let time: TimeInterval = 199160
        let duration: TimeInterval = 199
        let track = iTunesProvider.Track(info: ["Total Time":time])
        XCTAssertEqual(duration, track.totalTime, "Time calculation wrong!")
    }
    func testStoreURL() {
        let str = "itms://itunes.com/link?n=Numb&an=Linkin%20Park&pn=Meteora"
        let url = URL(string: str)
        let track = iTunesProvider.Track(info: ["Name":"Numb", "Artist":"Linkin Park", "Album":"Meteora"])
        XCTAssertEqual(url, track.storeURL, "Wrong Store URL")
    }
}
