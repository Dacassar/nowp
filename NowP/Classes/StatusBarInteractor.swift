//
//  StatusBarHandler.swift
//  NowP
//
//  Created by Евгений Браницкий on 10.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

protocol StatusBarInteractor {}

class Interactor: StatusBarInteractor {
    private let presenter: StatusBarPresenter = Presenter.assemblePresenter()
}


