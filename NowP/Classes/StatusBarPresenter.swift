//
//  StatusBarPresenter.swift
//  NowP
//
//  Created by Евгений Браницкий on 12.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

protocol StatusBarPresenter {
    func leftClick(_ sender: NSControl)
    func rightClick(_ sender: NSControl)
    static func assemblePresenter() -> StatusBarPresenter
}

class Presenter: StatusBarPresenter {
    private var statusBarItem: NSStatusItem!
    private var popover: NSPopover!
    
    private func createStatusBarItem() {
        let icon = #imageLiteral(resourceName: "icon_status_bar")
        let button = StatusBarButton(frame: NSRect(origin: NSPoint.zero, size: icon.size))
        button.imagePosition = .imageOnly
        button.image = icon
        button.imageScaling = .scaleProportionallyDown
        button.isBordered = false
        button.setButtonType(.momentaryChange)
        button.target = self
        button.action = #selector(Presenter.leftClick(_:))
        button.rightAction = #selector(Presenter.rightClick(_:))
        
        let item = NSStatusBar.system().statusItem(withLength: NSVariableStatusItemLength)
        item.view = button
        statusBarItem = item
    }

    private func createPopover() {
        let popover = NSPopover()
        popover.behavior = .transient
        popover.animates = true
        let controller = ControlInitializer().viewController!
        popover.contentViewController = controller
        self.popover = popover
    }
    
    // MARK: - Status Bar Presenter Protocol Conformance
    
    static func assemblePresenter() -> StatusBarPresenter {
        let presenter = Presenter()
        presenter.createStatusBarItem()
        presenter.createPopover()
        return presenter
    }
    
    dynamic func leftClick(_ sender: NSControl) {
        popover.show(relativeTo: sender.bounds, of: sender, preferredEdge: .minY)
    }
    
    dynamic func rightClick(_ sender: NSControl) {
        
    }
}

fileprivate class StatusBarButton: NSButton {
    var rightAction: Selector?
    
    fileprivate override func rightMouseDown(with event: NSEvent) {
        guard let target = target, let rightAction = rightAction else { return }
        Timer.scheduledTimer(timeInterval: 0, target: target, selector: rightAction, userInfo: nil, repeats: false)
    }
}
