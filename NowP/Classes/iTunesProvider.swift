//
//  iTunesProvider.swift
//  NowP
//
//  Created by Евгений Браницкий on 10.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

class iTunesProvider {

    struct Track {
        fileprivate enum Keys: String {
            case albumRating = "Album Rating"
            case albumRatingKind = "Album Rating Kind"
            case artworkCount = "Artwork Count"
            case storeURL = "Store URL"
            case skipCount = "Skip Count"
            case year = "Year"
            case genre = "Genre"
            case displayLine_1 = "Display Line 1"
            case location = "Location"
            case name = "Name"
            case persistentId = "PersistentID"
            case libraryPersistentId = "Library PersistentID"
            case artist = "Artist"
            case playCount = "Play Count"
            case album = "Album"
            case albumRatingComputed = "Album Rating Computed"
            case likeStatus = "Like Status"
            case displayLine_0 = "Display Line 0"
            case totalTime = "Total Time"
            case trackNumber = "Track Number"
            case trackCount = "Track Count"
            case ratingComputed = "Rating Computed"
            case albumArtist = "Album Artist"
        }
        private enum RatingKind: String {
            case computed = "kRtC"
            case userSpecified = "kRtU"
        }
        private let rawInfo: [AnyHashable: Any]
        private let id: Int
        init(info: [AnyHashable: Any], trackId: Int = 0) {
            rawInfo = info
            id = trackId
        }
        private func valueFor<T>(_ key: Keys, defaultValue: T) -> T {
            guard let value = rawInfo[key.rawValue] as? T else { return defaultValue }
            return value
        }
        var albumRating: Float {
            let result: Float = valueFor(.albumRating, defaultValue: 0)
            return result
        }
        var artworkCount: Int {
            let result: Int = valueFor(.artworkCount, defaultValue: 0)
            return result
        }
        var storeURL: URL? {
            let urlString: String = valueFor(.storeURL, defaultValue: "")
            guard !urlString.isEmpty, let result = URL(string: urlString) else {
                var components = URLComponents()
                components.scheme = "itms"
                components.host = "itunes.com"
                components.path = "/link"
                let nameItem = URLQueryItem(name: "n", value: name)
                let artistItem = URLQueryItem(name: "an", value: artist)
                let albumItem = URLQueryItem(name: "pn", value: album)
                components.queryItems = [nameItem, artistItem, albumItem]
                return components.url
            }
            return result
        }
        var skipCount: Int {
            let result: Int = valueFor(.skipCount, defaultValue: 0)
            return result
        }
        var year: Int {
            let result: Int = valueFor(.year, defaultValue: 0)
            return result
        }
        var genre: String {
            let result: String = valueFor(.genre, defaultValue: "")
            return result
        }
        var name: String {
            let result: String = valueFor(.name, defaultValue: "")
            return result
        }
        var location: URL? {
            let urlString: String = valueFor(.location, defaultValue: "")
            guard !urlString.isEmpty else { return nil }
            let result = URL(fileURLWithPath: urlString)
            return result
        }
        var trackCount: Int {
            let result: Int = valueFor(.trackCount, defaultValue: 0)
            return result
        }
        var ratingComputed: Float {
            let result: Float = valueFor(.ratingComputed, defaultValue: 0)
            return result
        }
        var totalTime: TimeInterval {
            let result: TimeInterval = valueFor(.totalTime, defaultValue: -1)
            if result == -1 {
                let str: String = valueFor(.totalTime, defaultValue: "")
                if !str.isEmpty {
                    var interval: TimeInterval = 0
                    let components = str.components(separatedBy: ":")
                    for (index, item) in components.reversed().enumerated() {
                        interval += (TimeInterval(item) ?? 0) * pow(60, Double(index))
                    }
                    return interval
                }
            }
            return TimeInterval(Int(result / 1000))
        }
        var persistentID: Int64 {
            let result: Int64 = valueFor(.persistentId, defaultValue: 0)
            return result
        }
        var libraryPersistentId: Int64 {
            let result: Int64 = valueFor(.libraryPersistentId, defaultValue: 0)
            return result
        }
        var artist: String {
            let result: String = valueFor(.artist, defaultValue: "")
            return result
        }
        var playCount: Int {
            let result: Int = valueFor(.playCount, defaultValue: 0)
            return result
        }
        var album: String {
            let result: String = valueFor(.album, defaultValue: "")
            return result
        }
        var trackNumber: Int {
            let result: Int = valueFor(.trackNumber, defaultValue: 0)
            return result
        }
        var albumArtist: String {
            let result: String = valueFor(.albumArtist, defaultValue: "")
            return result
        }
        var albumRatingComputed: Float {
            var result: Float
            let kindStr: String = valueFor(.albumRatingKind, defaultValue: "")
            if !kindStr.isEmpty {
                result = valueFor(.albumRating, defaultValue: 0)
            }
            else {
                result = valueFor(.albumRatingComputed, defaultValue: 0)
            }
            return result
        }
        var displayLine0: String {
            let result: String = valueFor(.displayLine_0, defaultValue: name)
            return result
        }
        var displayLine1: String {
            let result: String = valueFor(.displayLine_1, defaultValue: artist + " — " + album)
            return result
        }
        var artwork: NSImage?
    }
    
    enum Command: String {
        case play
        case pause
        case stop
        case next = "next track"
        case back = "back track"
    }
    
    private enum PrivateCommand: String {
        case playerState = "return player state"
        case playerPosition = "return player position"
        case currentTrackInfo = "set aTrack to the current track\nset json to {|Album Rating|:album rating of aTrack, |Artwork Count|:count of artwork of aTrack, |Skip Count|:skipped count of aTrack, |Year|:year of aTrack, |Genre|:genre of aTrack, |Name|:name of aTrack, |Skip Date|:skipped date of aTrack, |Track Count|:track count of aTrack, |Rating|:rating of aTrack, |Rating Kind|:rating kind of aTrack, |Total Time|:time of aTrack, |PersistentID|:persistent ID of aTrack, |Artist|:artist of aTrack, |Play Count|:played count of aTrack, |Album|:album of aTrack, |Track Number|:track number of aTrack, |Album Artist|:album artist of aTrack, |Album Rating Kind|:album rating kind of aTrack}\nif class of aTrack is file track then\nset json to json & {|Location|:location of aTrack}\nend if\nreturn json"
        case currentTrackImageData = "set aTrack to current track\nif (count of artwork of aTrack) > 0 then\ntell artwork 1 of aTrack\nreturn raw data\nend tell\nend if"
        case currentTrackId = "return id of current track"
    }
    
    enum PlayerState: String {
        case unknown
        case stopped = "kPSS"
        case playing = "kPSP"
        case paused = "kPSp"
        case fastForwarding = "kPSF"
        case rewinding = "kPSR"
    }
    
    private var itunesObserver: NSObjectProtocol?
    private var currentTrackId: Int = 0
    private var lastPlayerState: PlayerState = .unknown
    var didChangeTrack: ((Track?) -> Void)?
    var didChangeState: ((PlayerState) -> Void)?
    init() {
        itunesObserver = DistributedNotificationCenter.default().addObserver(forName: Notification.Name("com.apple.iTunes.playerInfo"), object: nil, queue: OperationQueue.main) { [unowned self] note in
            if let info = note.userInfo {
                let id: Int = self.performPrivateCommand(.currentTrackId) ?? 0
                if id != self.currentTrackId {
                    self.currentTrack = Track(info: info, trackId: id)
                    if let data: Data = self.performPrivateCommand(.currentTrackImageData) {
                        self.currentTrack?.artwork = NSImage(data: data)
                    }
                    self.currentTrackId = id
                    if let didChangeTrack = self.didChangeTrack {
                        didChangeTrack(self.currentTrack)
                    }
                }
            }
            else {
                self.currentTrackId = 0
                self.currentTrack = nil
                if let didChangeTrack = self.didChangeTrack {
                    didChangeTrack(nil)
                }
            }
            let state = self.playerState
            if state != self.lastPlayerState {
                self.lastPlayerState = state
                if let didChangeState = self.didChangeState {
                    didChangeState(state)
                }
            }
        }
    }
    var playerState: PlayerState {
        if let stateString: String = performPrivateCommand(.playerState) {
            return PlayerState(rawValue: stateString) ?? .unknown
        }
        return .unknown
    }
    var currentTrackPosition: TimeInterval {
        guard let position: Double = performPrivateCommand(.playerPosition) else {
            return TimeInterval(CGFloat.greatestFiniteMagnitude)
        }
        return position
    }
    
    var currentTrack: Track?
    
    // MARK: - Performing player commands
    
    func performCommand(_ command: Command) {
        let _: Any? = performScript(command.rawValue)
    }
    
    // MARK: - Performing private scripts
    
    private func performPrivateCommand<T>(_ command: PrivateCommand) -> T? {
        let result: T? = performScript(command.rawValue)
        return result
    }
    
    private func performScript<T>(_ script: String) -> T? {
        let scriptString = "tell application \"iTunes\"\n\(script)\nend tell"
        guard let script = NSAppleScript(source: scriptString) else { return nil }
        var error: NSDictionary?
        let result = script.executeAndReturnError(&error)
        if result == NSAppleEventDescriptor.null() || error != nil {
            return nil
        }
        else {
            if T.self is String.Type {
                return result.stringValue as? T
            }
            else if T.self is Double.Type {
                return result.doubleValue as? T
            }
            else if T.self is Int.Type {
                return Int(result.int32Value) as? T
            }
            else if T.self is Int32.Type {
                return result.int32Value as? T
            }
            else if T.self is Data.Type {
                return result.data as? T
            }
            else if T.self is [AnyHashable: Any].Type {
                do {
                    let dict = try result.objectRepresentation()
                    return dict as? T
                }
                catch { return nil }
            }
            return nil
        }
    }
    
    // MARK: - Fetching current track info
    
    func fetchCurrentTrack() -> Track? {
        guard let info: [AnyHashable: Any] = performPrivateCommand(.currentTrackInfo) else { return nil }
        let track = Track(info: info)
        currentTrack = track
        if let data: Data = performPrivateCommand(.currentTrackImageData) {
            currentTrack?.artwork = NSImage(data: data)
        }
        currentTrackId = performPrivateCommand(.currentTrackId) ?? 0
        if let didChangeTrack = didChangeTrack {
            didChangeTrack(currentTrack)
        }
        return track
    }
}

extension NSAppleEventDescriptor {
    enum CastingError: Error {
        case toObject
        case toDictionary
    }
    func objectRepresentation() throws -> Any {
        let aeDescription = aeDesc
        guard let descriptionType = aeDescription?.pointee.descriptorType else { throw CastingError.toObject }
        var result: Any?
        switch descriptionType {
        case typeAEList:
            var res = [Any]()
            for i in (1 ..< numberOfItems+1) {
                do {
                    if let item = try atIndex(i)?.objectRepresentation() {
                        res.append(item)
                    }
                }
                catch { continue }
            }
            result = res
            break
        case typeAERecord:
            do {
                result = try dictionaryRepresentation()
            }
            catch { throw error }
            break
        case typeTrue:
            result = true
            break
        case typeFalse:
            result = false
            break
        case typeNull:
            result = NSNull()
            break
        case typeChar: fallthrough
        case "utxt".fourCharCode:
            result = stringValue
            break
        case "long".fourCharCode: fallthrough
        case "shor".fourCharCode:
            result = int32Value
            break
        case "doub".fourCharCode: fallthrough
        case "sing".fourCharCode:
            result = doubleValue
            break
        case "bool".fourCharCode:
            result = booleanValue
            break
        case "type".fourCharCode:
            result = NSNull()
            break
        case "alis".fourCharCode:
            result = fileURLValue
            break
        case "ldt ".fourCharCode:
            result = dateValue
            break
        default:
            result = stringValue
            break
        }
        if let result = result {
            return result
        }
        else {
            throw CastingError.toObject
        }
    }
    
    private func dictionaryRepresentation() throws -> [AnyHashable: Any] {
        var result = [AnyHashable: Any]()
        for i in (1 ..< numberOfItems + 1) {
            let keyword = keywordForDescriptor(at: i)
            if keyword == "usrf".fourCharCode {
                if let list = paramDescriptor(forKeyword: keyword) {
                    var j: Int = 0
                    while j <= list.numberOfItems {
                        do {
                            j += 1
                            let k = try list.atIndex(j)?.objectRepresentation()
                            j += 1
                            let v = try list.atIndex(j)?.objectRepresentation()
                            guard let key = k as? AnyHashable, let value = v else {
                                break
                            }
                            result[key] = value
                        }
                        catch { throw error }
                    }
                }
            }
            else {
                do {
                    let key = String.withFourCharCode(keyword)
                    if let value = try paramDescriptor(forKeyword: keyword)?.objectRepresentation() {
                        result[key] = value
                    }
                }
            }
        }
        return result
    }
}
