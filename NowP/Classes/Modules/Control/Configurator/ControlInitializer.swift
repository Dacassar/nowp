//
//  ControlInitializer.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 14.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Foundation

class ControlInitializer {
    let viewController: ControlViewController!

    init() {
        let configurator = ControlConfigurator()
        viewController = ControlViewController()
        configurator.configureModuleForViewInput(viewInput: viewController)
        viewController.setupInitialState()
    }
}
