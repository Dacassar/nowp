//
//  ControlConfigurator.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 14.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

class ControlConfigurator {
    func configureModuleForViewInput<NSViewController>(viewInput: NSViewController) {
        if let viewController = viewInput as? ControlViewController {
            configure(viewController)
        }
    }
    private func configure(_ viewController: ControlViewController) {
        let presenter = ControlPresenter()
        presenter.view = viewController

        let interactor = ControlInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        interactor.prepareITunesProvider()
    }
}
