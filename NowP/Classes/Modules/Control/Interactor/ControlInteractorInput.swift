//
//  ControlInteractorInput.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 14.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Foundation

protocol ControlInteractorInput {
    func prepareITunesProvider()
    func fetchCurrentTrack()
    func playPause()
    func next()
    func prev()
}
