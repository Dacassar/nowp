//
//  ControlInteractorOutput.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 14.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

protocol ControlInteractorOutput: class {
    func updateWithTrack(_ track: iTunesProvider.Track?)
    func playerStateChanged(to: iTunesProvider.PlayerState)
}
