//
//  ControlInteractor.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 14.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Foundation

class ControlInteractor: ControlInteractorInput {
    weak var output: ControlInteractorOutput!
    private let itunes = iTunesProvider()

    func prepareITunesProvider() {
        itunes.didChangeTrack = { [unowned self] track in
            self.output.updateWithTrack(track)
        }
        itunes.didChangeState = { [unowned self] state in
            self.output.playerStateChanged(to: state)
        }
    }

    func fetchCurrentTrack() {
        let _ = itunes.fetchCurrentTrack()
        output.playerStateChanged(to: itunes.playerState)
    }

    func playPause() {
        switch itunes.playerState {
        case .paused, .stopped: itunes.performCommand(.play); break
        case .playing: itunes.performCommand(.pause); break
        default: break
        }
    }

    func next() {
        itunes.performCommand(.next)
    }

    func prev() {
        itunes.performCommand(.back)
    }
}
