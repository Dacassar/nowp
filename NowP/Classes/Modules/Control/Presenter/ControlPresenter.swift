//
//  ControlPresenter.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 14.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

class ControlPresenter: ControlModuleInput, ControlViewOutput, ControlInteractorOutput {
    weak var view: ControlViewInput!
    var interactor: ControlInteractorInput!
    var firstAppear = true

    func viewIsReady() {
        interactor.fetchCurrentTrack()
    }

    func viewWillAppear() {
        if firstAppear {
            firstAppear = false
            view.setupButtonsLayer()
            view.setupTextLayer()
        }
    }

    func playPausePressed() {
        interactor.playPause()
    }

    func backwardPressed() {
        interactor.prev()
    }

    func forwardPressed() {
        interactor.next()
    }

    func infoPressed() {
        
    }

    func playerStateChanged(to: iTunesProvider.PlayerState) {
        view.setPlayerState(to)
    }
    
    func updateWithTrack(_ track: iTunesProvider.Track?) {
        guard let track = track else { return }
        view.setArtistText(track.artist)
        view.setTitleText(track.name)
        view.setArtwork(track.artwork)
    }
}
