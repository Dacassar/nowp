//
//  ControlPresenterInput.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 14.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Foundation

protocol ControlModuleInput {
    func playerStateChanged(to: iTunesProvider.PlayerState)
    func updateWithTrack(_ track: iTunesProvider.Track?)
}
