//
//  ControlViewOutput.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 14.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Foundation

protocol ControlViewOutput {
    func viewIsReady()
    func viewWillAppear()
    func playPausePressed()
    func forwardPressed()
    func backwardPressed()
    func infoPressed()
}
