//
//  ControlViewController.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 14.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa
import SnapKit

class ControlViewController: NSViewController, ControlViewInput {
    var output: ControlViewOutput!

    private weak var artworkView: NSImageView!
    private weak var playPauseButton: NSButton!
    private weak var prevButton: NSButton!
    private weak var nextButton: NSButton!
    private weak var playPauseLayer: CALayer!
    private weak var controlContainer: NSView!
    
    private weak var artistLayer: TextLayer!
    private weak var titleLayer: TextLayer!
    private weak var infoContainer: NSView!
    
    private var artistText: String?
    private var titleText: String?

    private func setupArtworkView(in v: NSView) {
        let imageView = NSImageView()
        imageView.imageScaling = .scaleNone
        v.addSubview(imageView)
        imageView.snp.makeConstraints({ maker in
            maker.size.equalTo(CGSize(width: 200, height: 200))
            maker.edges.equalTo(NSEdgeInsetsZero)
        })
        artworkView = imageView
    }

    private func setupControlView(in v: NSView) {
        let container = NSView()
        container.alphaValue = 0.25

        let play = NSButton()
        play.setButtonType(.momentaryChange)
        play.title = ""
        play.isBordered = false
        container.addSubview(play)
        play.snp.makeConstraints { maker in
            maker.center.equalToSuperview()
            maker.height.equalToSuperview()
            maker.width.equalTo(play.snp.height)
        }
        play.target = self
        play.action = #selector(ControlViewController.playPausePressed)
        play.wantsLayer = true
        play.layer?.backgroundColor = NSColor.clear.cgColor
        playPauseButton = play

        let back = NSButton()
        back.setButtonType(.momentaryChange)
        back.title = ""
        back.isBordered = false
        container.addSubview(back)
        back.snp.makeConstraints { maker in
            maker.centerY.equalToSuperview()
            maker.height.equalToSuperview()
            maker.width.equalTo(back.snp.height)
            maker.right.equalTo(play.snp.left)
        }
        back.target = self
        back.action = #selector(ControlViewController.backwardPressed)
        prevButton = back

        let forward = NSButton()
        forward.setButtonType(.momentaryChange)
        forward.title = ""
        forward.isBordered = false
        container.addSubview(forward)
        forward.snp.makeConstraints { maker in
            maker.centerY.equalToSuperview()
            maker.height.equalToSuperview()
            maker.width.equalTo(forward.snp.height)
            maker.left.equalTo(play.snp.right)
        }
        forward.target = self
        forward.action = #selector(ControlViewController.forwardPressed)
        nextButton = forward

        v.addSubview(container)
        container.snp.makeConstraints { maker in
            maker.leading.equalTo(0)
            maker.trailing.equalTo(0)
            maker.height.equalTo(40)
            maker.bottom.equalTo(0)
        }
        controlContainer = container
        
        let layersContainer = NSView()
        layersContainer.backgroundColor = NSColor.clear
        layersContainer.alphaValue = 0.25
        v.addSubview(layersContainer)
        layersContainer.snp.makeConstraints { maker in
            maker.top.equalTo(0)
            maker.leading.equalTo(0)
            maker.trailing.equalTo(0)
            maker.height.equalTo(0)
        }
        infoContainer = layersContainer
    }

    override func viewWillAppear() {
        super.viewWillAppear()
        output.viewWillAppear()
    }

    // MARK: - Actions
    private dynamic func backwardPressed() {
        output.backwardPressed()
    }

    private dynamic func playPausePressed() {
        output.playPausePressed()
    }

    private dynamic func forwardPressed() {
        output.forwardPressed()
    }

    private dynamic func infoPressed() {
        output.infoPressed()
    }

    private func layerForButton(_ button: NSButton) -> CALayer {
        let layer = CALayer()
        layer.backgroundColor = NSColor.clear.cgColor
        layer.frame = button.frame.insetBy(dx: 5, dy: 5)
        return layer
    }

    // MARK: - ControlView Input
    func setupInitialState() {
        let v = ControlContainer()
        v.mouseDidEnter = { [unowned self] _ in
            let backColor = NSColor.white.withAlphaComponent(0.75)
            self.controlContainer.setVibrancy(radius: 2.5)
            self.controlContainer.animator().alphaValue = 1
            self.controlContainer.animator().backgroundColor = backColor
            self.infoContainer.setVibrancy(radius: 2.5)
            self.infoContainer.animator().alphaValue = 1
            self.infoContainer.animator().backgroundColor = backColor
        }
        v.mouseDidExit = { [unowned self] _ in
            self.controlContainer.setVibrancy(radius: 0)
            self.controlContainer.animator().alphaValue = 0.25
            self.controlContainer.animator().backgroundColor = NSColor.clear
            self.infoContainer.setVibrancy(radius: 0)
            self.infoContainer.animator().alphaValue = 0.25
            self.infoContainer.animator().backgroundColor = NSColor.clear
        }
        setupArtworkView(in: v)
        setupControlView(in: v)
        view = v
        output.viewIsReady()
    }

    func setupButtonsLayer() {
        for (index, button) in [prevButton, playPauseButton, nextButton].enumerated() {
            var image: CGImage?
            switch index {
            case 0: image = #imageLiteral(resourceName: "icon_prev").cgImage; break
            case 1: image = #imageLiteral(resourceName: "icon_play").cgImage; break
            case 2: image = #imageLiteral(resourceName: "icon_next").cgImage; break
            default: break
            }
            let layer = layerForButton(button!)
            layer.contents = image
            if (index == 1) {
                playPauseLayer = layer
            }
            button!.superview?.layer?.addSublayer(layer)
        }
    }
    
    func setupTextLayer() {
        infoContainer.wantsLayer = true

        let aLayer = TextLayer()
        aLayer.color = NSColor.RGB(red: 53, green: 52, blue: 51)
        infoContainer.layer?.addSublayer(aLayer)
        artistLayer = aLayer
        
        let nLayer = TextLayer()
        nLayer.color = NSColor.RGB(red: 53, green: 52, blue: 51)
        infoContainer.layer?.addSublayer(nLayer)
        titleLayer = nLayer
        
        layoutInfoLayers()
        artistLayer.setText(artistText)
        titleLayer.setText(titleText)
    }

    func setArtwork(_ image: NSImage?) {
        let img = image?.imageScaledToEdgeValue(200, edgeRelation: .min)
        artworkView?.image = img
    }
    
    func setArtistText(_ artist: String?) {
        artistText = artist
        if let layer = artistLayer {
            layer.setText(artist)
        }
    }
    
    func setTitleText(_ title: String?) {
        titleText = title
        if let layer = titleLayer {
            layer.setText(title)
        }
    }

    func setPlayerState(_ state: iTunesProvider.PlayerState) {
        guard playPauseLayer != nil else { return }
        var contents: Any?
        switch state {
        case .playing: contents = #imageLiteral(resourceName: "icon_pause").cgImage; break
        default: contents = #imageLiteral(resourceName: "icon_play").cgImage; break
        }
        let transition = CATransition()
        transition.type = kCATransitionFade
        transition.duration = 0.3
        CATransaction.begin()
        CATransaction.setCompletionBlock({ [unowned self] in
            self.playPauseLayer.contents = contents
            self.playPauseLayer.add(transition, forKey: kCATransition)
            })
        CATransaction.commit()
    }
    
    func layoutInfoLayers() {
        guard let artistLayer = artistLayer, let titleLayer = titleLayer else { return }

        let artistHeight = artistText?.size(with: artistLayer.font).height ?? 0
        artistLayer.isHidden = artistHeight == 0
        artistLayer.frame = CGRect(x: 8, y: 8, width: artistLayer.superlayer!.frame.width - 16, height: artistHeight)

        let titleHeight = titleText?.size(with: titleLayer.font).height ?? 0
        titleLayer.isHidden = titleHeight == 0
        titleLayer.frame = CGRect(x: 8, y: artistLayer.frame.origin.y + artistLayer.frame.height, width: artistLayer.superlayer!.frame.width - 16, height: titleHeight)

        if infoContainer.frame.height != artistHeight + titleHeight {
            infoContainer.snp.updateConstraints({ maker in
                maker.height.equalTo(artistHeight + titleHeight + 16)
            })
        }
        infoContainer.isHidden = artistHeight == 0 && titleHeight == 0
    }
}
