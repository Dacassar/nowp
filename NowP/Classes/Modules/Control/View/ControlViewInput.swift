//
//  ControlInput.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 14.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

protocol ControlViewInput: class {
    func setupInitialState()
    func setArtwork(_ image: NSImage?)
    func setPlayerState(_ state: iTunesProvider.PlayerState)
    func setupButtonsLayer()
    func setupTextLayer()
    func setArtistText(_ artist: String?)
    func setTitleText(_ title: String?)
    func layoutInfoLayers()
}
