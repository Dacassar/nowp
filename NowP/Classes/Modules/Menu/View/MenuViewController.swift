//
//  MenuMenuViewController.swift
//  nowp
//
//  Created by Yevgeniy Branitsky on 28/02/2017.
//  Copyright © 2017 Akki. All rights reserved.
//

import Cocoa

class MenuViewController: NSViewController, MenuViewInput {

    var output: MenuViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }


    // MARK: MenuViewInput
    func setupInitialState() {
    
    }
}
