//
//  MenuMenuViewInput.swift
//  nowp
//
//  Created by Yevgeniy Branitsky on 28/02/2017.
//  Copyright © 2017 Akki. All rights reserved.
//

protocol MenuViewInput: class {

    /**
        @author Yevgeniy Branitsky
        Setup initial state of the view
    */

    func setupInitialState()
}
