//
//  MenuMenuViewOutput.swift
//  nowp
//
//  Created by Yevgeniy Branitsky on 28/02/2017.
//  Copyright © 2017 Akki. All rights reserved.
//

protocol MenuViewOutput {

    /**
        @author Yevgeniy Branitsky
        Notify presenter that view is ready
    */

    func viewIsReady()
}
