//
//  MenuMenuInitializer.swift
//  nowp
//
//  Created by Yevgeniy Branitsky on 28/02/2017.
//  Copyright © 2017 Akki. All rights reserved.
//

import Cocoa

class MenuModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var menuViewController: MenuViewController!

    override func awakeFromNib() {

        let configurator = MenuModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: menuViewController)
    }

}
