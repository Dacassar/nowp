//
//  MenuMenuConfigurator.swift
//  nowp
//
//  Created by Yevgeniy Branitsky on 28/02/2017.
//  Copyright © 2017 Akki. All rights reserved.
//

import Cocoa

class MenuModuleConfigurator {

    func configureModuleForViewInput<NSViewController>(viewInput: NSViewController) {

        if let viewController = viewInput as? MenuViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: MenuViewController) {

        let router = MenuRouter()

        let presenter = MenuPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = MenuInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
