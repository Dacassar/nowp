//
//  MenuMenuPresenter.swift
//  nowp
//
//  Created by Yevgeniy Branitsky on 28/02/2017.
//  Copyright © 2017 Akki. All rights reserved.
//

class MenuPresenter: MenuModuleInput, MenuViewOutput, MenuInteractorOutput {

    weak var view: MenuViewInput!
    var interactor: MenuInteractorInput!
    var router: MenuRouterInput!

    func viewIsReady() {

    }
}
