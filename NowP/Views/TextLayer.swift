//
//  TextLayer.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 16.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

class TextLayer: CALayer {
    private weak var textLayer: CATextLayer!
    private var _font: NSFont!
    var color: NSColor {
        get {
            return NSColor(cgColor: textLayer.foregroundColor!)!
        }
        set {
            textLayer.foregroundColor = newValue.cgColor
        }
    }
    var font: NSFont {
        get {
            return _font
        }
        set {
            _font = newValue
            let name = newValue.fontName
            let size = newValue.fontDescriptor.object(forKey: NSFontSizeAttribute) as? CGFloat
            textLayer.font = name as CFTypeRef
            textLayer.fontSize = size ?? NSFont.systemFontSize()
        }
    }
    var alignment: NSTextAlignment = .center
    
    override init() {
        super.init()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        let layer = CATextLayer()
        layer.backgroundColor = NSColor.clear.cgColor
        addSublayer(layer)
        textLayer = layer
        font = NSFont.systemFont(ofSize: NSFont.systemFontSize())
        color = NSColor.white
    }

    func setText(_ text: String?) {
        guard let str = text else {
            textLayer.string = nil
            return
        }
        let rect = bounds
        let size = str.size(with: font)
        contentsRect = bounds
        if size.width <= rect.width {
            textLayer.removeAllAnimations()
            textLayer.alignmentMode = alignment(for: alignment)
            textLayer.frame = CGRect(origin: CGPoint.zero, size: rect.size)
        }
        else {
            textLayer.alignmentMode = kCAAlignmentNatural
            textLayer.frame = CGRect(origin: CGPoint.zero, size: size)
            textLayer.add(pulseAnimation(), forKey: "transform.translation.x")
        }
        textLayer.string = str
    }

    private func alignment(for mode: NSTextAlignment) -> String {
        switch mode {
        case .left: return kCAAlignmentLeft
        case .right: return kCAAlignmentRight
        case .center: return kCAAlignmentCenter
        case .natural: return kCAAlignmentNatural
        case .justified: return kCAAlignmentJustified
        }
    }

    private func pulseAnimation() -> CAAnimation {
        let animation = CABasicAnimation(keyPath: "transform.translation.x")
        let diff = textLayer.frame.width - frame.width
        animation.fromValue = 10
        animation.toValue = -diff - 10
        animation.speed = 0.1
        animation.autoreverses = true
        animation.repeatCount = HUGE
        return animation
    }
}
