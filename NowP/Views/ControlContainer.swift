//
//  ControlContainer.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 16.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

class ControlContainer: NSView {
    var mouseDidEnter: ((_ enterPoint: CGPoint) -> Void)?
    var mouseDidExit: ((_ exitPoint: CGPoint) -> Void)?
    private var trackingArea: NSTrackingArea?

    override func updateTrackingAreas() {
        if let area = trackingArea {
            removeTrackingArea(area)
            trackingArea = nil
        }
        trackingArea = NSTrackingArea(rect: bounds, options: [.activeAlways, .mouseEnteredAndExited], owner: self, userInfo: nil)
        addTrackingArea(trackingArea!)
    }

    override func mouseEntered(with event: NSEvent) {
        super.mouseEntered(with: event)
        if let didEnter = mouseDidEnter {
            didEnter(event.locationInWindow)
        }
    }
    override func mouseExited(with event: NSEvent) {
        super.mouseExited(with: event)
        if let didExit = mouseDidExit {
            didExit(event.locationInWindow)
        }
    }
}
