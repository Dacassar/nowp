//
//  NSViewExtension.swift
//  NowP
//
//  Created by Evgeniy Branitsky on 15.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

extension NSView {
    fileprivate struct Filter {
        static var value: CIFilter?
    }

    var backgroundColor: NSColor? {
        get {
            guard let ref = layer?.backgroundColor else { return nil }
            return NSColor(cgColor: ref)
        }
        set {
            if !wantsLayer {
                wantsLayer = true
            }
            layer?.backgroundColor = newValue?.cgColor
        }
    }

    private var vibrancyFilter: CIFilter? {
        return layer?.backgroundFilters?.filter({ ($0 as? CIFilter) == Filter.value }).first as? CIFilter
    }

    func setVibrancy(radius: CGFloat) {
        if !wantsLayer {
            wantsLayer = true
        }
        if !layerUsesCoreImageFilters {
            layerUsesCoreImageFilters = true
        }
        removeVibrancy()
        if let created = Filter.value {
            created.setValue(radius, forKey: kCIInputRadiusKey)
            layer?.backgroundFilters?.append(created)
        }
        else {
            Filter.value = createFilter(radius: radius)
            layer?.backgroundFilters?.append(Filter.value!)
        }
    }

    func removeVibrancy() {
        if let index = layer?.backgroundFilters?.index(where: { ($0 as? CIFilter) == Filter.value }) {
            layer?.backgroundFilters?.remove(at: index)
        }
    }

    private func createFilter(radius: CGFloat) -> CIFilter? {
        let blurFilter = CIFilter(name: "CIGaussianBlur")
        blurFilter?.setDefaults()
        blurFilter?.setValue(radius, forKey: kCIInputRadiusKey)
        return blurFilter
    }
}

extension NSVisualEffectView {
    private var _layerName: String {
        return "ClearCopyLayer"
    }
    var tintColor: NSColor? {
        get {
            if let colorLayer = layer?.sublayers?.filter({ $0.name == _layerName }).first {
                guard let ref = colorLayer.backgroundColor else { return nil }
                return NSColor(cgColor: ref)
            }
            return nil
        }
        set {
            if let colorLayer = layer?.sublayers?.filter({ $0.name == _layerName }).first {
                colorLayer.backgroundColor = newValue?.cgColor
            }
        }
    }
}
