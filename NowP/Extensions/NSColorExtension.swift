//
//  NSColorExtension.swift
//  NowP
//
//  Created by Евгений Браницкий on 18.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

extension NSColor {
    static func RGB(red: UInt8, green: UInt8, blue: UInt8, alpha: Float = 1) -> NSColor {
        return NSColor(calibratedRed: CGFloat(red)/255, green: CGFloat(green)/255, blue: CGFloat(blue)/255, alpha: CGFloat(alpha))
    }
    static var random: NSColor {
        let hue: CGFloat = ( CGFloat(arc4random() % 256) / 256 );  //  0.0 to 1.0
        let saturation: CGFloat = ( CGFloat(arc4random() % 128) / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
        let brightness: CGFloat = ( CGFloat(arc4random() % 128) / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
        return NSColor(calibratedHue: hue, saturation: saturation, brightness: brightness, alpha: 1)
    }
}
