//
//  StringExtension.swift
//  NowP
//
//  Created by Евгений Браницкий on 11.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

extension String {
    var fourCharCode: FourCharCode {
        var result: FourCharCode = 0
        for char in utf16 {
            result = (result << 8) + FourCharCode(char)
        }
        return result
    }
    
    static func withFourCharCode(_ code: FourCharCode) -> String {
        let bytes = [ CChar(truncatingBitPattern: (code >> 24) & 0xFF),
                      CChar(truncatingBitPattern: (code >> 16) & 0xFF),
                      CChar(truncatingBitPattern: (code >> 8) & 0xFF),
                      CChar(truncatingBitPattern: code & 0xFF) ]
        return String(cString: bytes)
    }

    func size(with font: NSFont, alignment: NSTextAlignment = .left, lineBreak: NSLineBreakMode = .byWordWrapping, constrainedSize: CGSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)) -> CGSize {
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineBreakMode = lineBreak
        paragraph.alignment = alignment
        let attributes = [NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraph]
        let options: NSStringDrawingOptions = [.usesFontLeading, .usesLineFragmentOrigin]
        let frame = (self as NSString).boundingRect(with: constrainedSize, options: options, attributes: attributes, context: nil)
        return frame.size
    }
}
