//
//  ImageExtension.swift
//  NowP
//
//  Created by Евгений Браницкий on 13.02.17.
//  Copyright © 2017 Evgeniy Branitsky. All rights reserved.
//

import Cocoa

extension NSImage {
    enum EdgeRelation {
        case min
        case max
    }
    func imageScaledToEdgeValue(_ value: CGFloat, edgeRelation: EdgeRelation) -> NSImage? {
        switch edgeRelation {
        case .min: guard min(size.width, size.height) != value else { return self }; break
        case .max: guard max(size.width, size.height) != value else { return self }; break
        }
        guard let cgImage = self.cgImage(forProposedRect: nil, context: nil, hints: nil) else { return nil }
        let bitmap = NSBitmapImageRep(cgImage: cgImage)
        guard let data = bitmap.representation(using: .PNG, properties: [:]) else { return nil }
        if let source = CGImageSourceCreateWithData(data as CFData, nil) {
            var sizeValue = value
            if edgeRelation == .min {
                sizeValue = value * max(size.width, size.height) / min(size.width, size.height)
            }
            let options: [NSString: AnyObject] = [
                kCGImageSourceThumbnailMaxPixelSize: sizeValue as AnyObject,
                kCGImageSourceCreateThumbnailFromImageAlways: true as AnyObject
            ]
            let image = CGImageSourceCreateThumbnailAtIndex(source, 0, options as CFDictionary).flatMap({ NSImage(cgImage: $0, size: NSSize.zero) })
            return image
        }
        return nil
    }
    var cgImage: CGImage {
        let data = tiffRepresentation
        let source = CGImageSourceCreateWithData(data as! CFData, nil)
        let maskRef = CGImageSourceCreateImageAtIndex(source!, 0, nil)
        return maskRef!
    }
}
